<?php

function guzzle_oauth_login_register_settings($form, &$form_state) {
  $query = db_select('guzzle_oauth_consumer_instance', 'c')
    ->fields('c', array('id', 'title'));
  $options = $query->execute()->fetchAllKeyed();
  $form['consumer_instances'] = array(
    '#type' => 'fieldset',
    '#title' => t('Consumers'),
  );
  $form['consumer_instances']['go_login_register_consumers_instances'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable Login and Register on this consumer instances.'),
    '#description' => t('Select no oAuth consumers to enable all consumers.'),
    '#default_value' => variable_get('go_login_register_consumers_instances', array()),
    '#options' => $options,
  );
  $form['show_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show links'),
  );
  $form['show_links']['go_login_register_add_links_to_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add login links to login form.'),
    '#description' => t('Add login links to the form shown on @url.', array('@url' => url('user/login'))),
    '#default_value' => variable_get('go_login_register_add_links_to_login', TRUE),
  );
  $form['show_links']['go_login_register_add_links_to_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add register links to register form.'),
    '#description' => t('Add register links to the form shown on @url.', array('@url' => url('user/register'))),
    '#default_value' => variable_get('go_login_register_add_links_to_register', TRUE),
  );
  $form['show_links']['go_login_register_add_links_to_login_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add login links to login block.'),
    '#description' => t('Add login links to the form shown on the login block.'),
    '#default_value' => variable_get('go_login_register_add_links_to_login_block', TRUE),
  );
  $form['register'] = array(
    '#type' => 'fieldset',
    '#title' => t('Registration'),
  );
  $form['register']['go_login_register_enable_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable User Registration.'),
    '#description' => t('Enable user registration via oAuth providers.'),
    '#default_value' => variable_get('go_login_register_enable_register', TRUE),
  );
  $states = array(
    'visible' => array(
      ':input[name="go_login_register_enable_register"]' => array('checked' => TRUE),
    ),
  );
  $form['register']['go_login_register_require_valid_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require a valid e-mail address.'),
    '#description' => t('On registration, when the provider does not provide an e-mail address, ask the user for a e-mail address. Disabling this option could lead to users in your database without a valid e-mail address.'),
    '#default_value' => variable_get('go_login_register_require_valid_email', FALSE),
    '#states' => $states,
  );
  $form['register_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site registration settings.'),
  );
  $form['register_settings']['go_login_register_follow_site_register_settings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Follow the site registration settings.'),
    '#description' => t('Follow the site registration settings as configured on !link. Disabling this option means a user is approved and logged in immediately after registration or login via oAuth. Enabling could mean a user needs to verify a e-mail address or needs administor approval when configured so on !link.', array('!link' => l(t('Account settings'), 'admin/config/people/accounts'))),
    '#default_value' => variable_get('go_login_register_follow_site_register_settings', FALSE),
  );
  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail'),
    '#description' => t('These settings will only work when the oAuth consumers can access the e-mail address from the remote account. Some oAuth providers will never provide e-mail addresses, while others need special configuration. Facebook for example needs the scope "email". Please read the documentation of the oAuth providers.')
  );
  $form['email']['go_login_register_match_on_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Match user on e-mail address.'),
    '#description' => t('On login, when no connected user is found, fallback to match on e-mail address.'),
    '#default_value' => variable_get('go_login_register_match_on_email', TRUE),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return system_settings_form($form);
}
