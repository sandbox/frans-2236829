<?php

$plugin = array(
  'post callback callback' => 'guzzle_oauth_login_register_post_callback_callback',
  'access callback' => 'user_is_anonymous',
  'access arguments' => array(),
);

// Callbacks are defined in guzzle_oauth.module.

function guzzle_oauth_login_register_post_callback_callback($access_token, $client, $request_token, $consumer_instance, $action) {
  if (user_is_logged_in()) {
    drupal_set_message(t('You are already logged in with another user. Logout first.'));
  }
  try {
    $info_client = guzzle_oauth_get_client_by_instance($consumer_instance, $access_token);
    $info = $info_client->getUserInfo();
    $external_id = $info_client->getUserId($info);

    // find account by Id.
    $account = user_external_load($consumer_instance->consumer . '_' . $external_id);
    $existing_account = NULL;
    if (module_exists('guzzle_oauth_accounts')) {
      $query = db_select('guzzle_oauth_accounts', 'a')
        ->fields('a', array('id', 'access_token','entity_id'))
        ->condition('a.entity_type', 'user')
        ->condition('a.remote_account_id', $external_id);
      $query->innerJoin('guzzle_oauth_consumer_instance', 'i', 'i.id = a.consumer_instance_id');
      $query->condition('i.consumer', $consumer_instance->consumer);
      $query->range(0, 1);
      $existing_account = $query->execute()->fetch();
      if (!$account && $existing_account) {
        $account = user_load($existing_account->entity_id);
      }
    }

    // find account by email
    if (!$account && variable_get('go_login_register_match_on_email', TRUE)) {
      $email = $info_client->getUserEmail($info);
      if ($email) {
        $account = user_load_by_mail($email);
      }
    }

    // register user
    if (!$account &&
        variable_get('go_login_register_enable_register', TRUE) &&
        (!variable_get('go_login_register_follow_site_register_settings', FALSE) ||
         (variable_get('go_login_register_follow_site_register_settings', FALSE) && variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) != USER_REGISTER_ADMINISTRATORS_ONLY)
        )
       ) {
      $email = $info_client->getUserEmail($info);
      if (!$email && variable_get('go_login_register_require_valid_email', FALSE)) {
        // TODO
        // Save $info and $consumer_instance in the session and redirect to email form.
      }
      if (!$email) {
        $email = $external_id . '@' . $consumer_instance->consumer . '.fake';
      }
      $password = user_password(8);
      $fields = array(
        'name' => $info_client->getUserLabel($info),
        'mail' => $email,
        'pass' => $password,
        'status' => (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) == USER_REGISTER_VISITORS),
        'init' => $external_id . '@' . $consumer_instance->consumer . '.fake',
        'roles' => array(
          DRUPAL_AUTHENTICATED_RID => 'authenticated user',
        ),
      );
      $account = user_save('', $fields);
    }
dpm ($account);
dpm ('woop');

    if ($account) {
      // Save user id info.
      $authmaps = array('authname_guzzle_oauth_' . $consumer_instance->consumer => $consumer_instance->consumer . '_' . $external_id);
      user_set_authmaps($account, $authmaps);

      // save in accounts if we can.
      if (module_exists('guzzle_oauth_accounts')) {
        $id = NULL;
        $existing_access_token = array();
        if ($existing_account) {
          $id = $existing_account->id;
          $existing_access_token = unserialize($existing_account->access_token);
          if (!is_array($existing_access_token)) {
            $existing_access_token = array();
          }
        }
        $connected_accounts = $info_client->getConnectedAccounts($info);
        foreach ($connected_accounts as $connected_account) {
          if ($connected_account['account_id'] == $external_id) {
            break;
          }
        }
        $save_token = array_merge($existing_access_token, $access_token);
        $expires = isset($connected_account['expires'])?$connected_account['expires']:0;
        if (!$expires && isset($save_token['expires_at'])) {
          $expires = $save_token['expires_at'];
        }
        $save = array(
          'id' => $id,
          'consumer_instance_id' => $consumer_instance->id,
          'remote_account_id' => $connected_account['account_id'],
          'remote_account_label' => $connected_account['account_label'],
          'remote_account_type' => $connected_account['account_type'],
          'access_token' => $save_token,
          'expires' => $expires,
          'entity_type' => 'user',
          'entity_id' => $account->uid,
        );
        $pk = array();
        if ($id) {
          $pk[] = 'id';
        }
        drupal_write_record('guzzle_oauth_accounts', $save, $pk);
      }

      // check if the user is enabled.
      if ($account->status) {
        if (variable_get('go_login_register_follow_site_register_settings', FALSE) && variable_get('user_email_verification', FALSE) && !$account->login) {
          drupal_set_message(t('Your account is currently pending e-mail verification. You have receveid a email with further instructions. !link to start a new e-mail verification.', array('!link' => l('Request a new password', 'user/password'))), 'warning');
        }
        else {
          $form_state['uid'] = $account->uid;
          user_login_submit(array(), $form_state);
          $_SESSION['GO_LOGGEDIN_VIA_CONSUMER_INSTANCE'] = $consumer_instance->id;
        }
      }
      else {
        if (variable_get('go_login_register_follow_site_register_settings', FALSE) && !$account->login && USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL == variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
          drupal_set_message(t('Your account is currently pending approval by the site administrator.'), 'warning');
        } elseif (!variable_get('go_login_register_follow_site_register_settings', FALSE) && !$account->login && USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL == variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
          // enable the user and log in.
          $account->status = 1;
          user_save($account);
          $form_state['uid'] = $account->uid;
          user_login_submit(array(), $form_state);
          $_SESSION['GO_LOGGEDIN_VIA_CONSUMER_INSTANCE'] = $consumer_instance->id;
        } else {
          drupal_set_message(t('Your account is blocked by the site administrator.'), 'warning');
        }
      }
    }
    else {
      $name = $info_client->getUserLabel($info);
      drupal_set_message(t('No user found for %service account %name', array('%service' => $consumer_instance->title, '%name' => $name)), 'warning');
    }
  } catch (Exception $e) {
    watchdog_exception('guzzle_oauth', $e);
    drupal_set_message(t('Something went wrong. Please try again.'), 'error');
  }

  // let the default callback do the redirect stuff.
  return guzzle_oauth_default_post_callback_callback($access_token, $client, $request_token, $consumer_instance, $action);
}